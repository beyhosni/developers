package com.jcdecaux.recruiting.developpers.domain.service;

import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import org.springframework.stereotype.Service;

@Service
public interface LanguageService {
    public LanguageEntity save(LanguageEntity language);

    public LanguageEntity get(Long id);

}

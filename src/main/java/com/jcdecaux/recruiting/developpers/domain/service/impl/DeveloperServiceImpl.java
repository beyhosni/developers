package com.jcdecaux.recruiting.developpers.domain.service.impl;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperLanguageDto;
import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import com.jcdecaux.recruiting.developpers.domain.repository.DeveloperRepository;
import com.jcdecaux.recruiting.developpers.domain.repository.LanguageRepository;
import com.jcdecaux.recruiting.developpers.domain.service.DeveloperService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    DeveloperRepository developerRepository;

    @Autowired
    LanguageRepository languageRepository;


    public DeveloperEntity save(DeveloperEntity developer){

        return developerRepository.save(developer);
    }

    public DeveloperEntity get(Long id){

        return null;
    }

    public DeveloperEntity save(DeveloperLanguageDto developerLanguageDto){
        DeveloperEntity developerEntity = developerRepository.findFirstById(developerLanguageDto.getIdDeveloper());
        List<LanguageEntity>  languages = new ArrayList<>() ;
        if(developerEntity.getLanguages() != null) {
        	languages = developerEntity.getLanguages();
        }
        for(Long id : developerLanguageDto.getIdLanguage()) {
        	LanguageEntity language = languageRepository.findFirstById(id);
        	if (language != null && !isExitingLanguage(languages , id)) {
            	languages.add(language);
        	}
        }
        
        developerEntity.setLanguages(languages);
        return developerRepository.save(developerEntity);
    }

    public List<DeveloperEntity> getDevsByLanguageId(Long languageId){
        LanguageEntity language = languageRepository.findFirstById(languageId);
        if (language != null) {
            return developerRepository.findAllByLanguages(language);
        } else {
        	return new ArrayList<DeveloperEntity>();
        }
    }
    
    private boolean isExitingLanguage(List<LanguageEntity>  languages, Long id) {
    	return languages.stream().anyMatch(l -> l.getId().equals(id));
    }
}

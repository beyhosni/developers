package com.jcdecaux.recruiting.developpers.domain.repository;

import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface DeveloperRepository extends Repository<DeveloperEntity, String>{

	@Query("SELECT i.id FROM DeveloperEntity i")
	String get();

	DeveloperEntity save(DeveloperEntity developerEntity);

	DeveloperEntity findFirstById (Long id);


	List<DeveloperEntity> findAllByLanguages (LanguageEntity languageEntity);


}

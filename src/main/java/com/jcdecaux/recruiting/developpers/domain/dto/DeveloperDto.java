package com.jcdecaux.recruiting.developpers.domain.dto;

import java.io.Serializable;
import java.util.List;

public class DeveloperDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String surName;
    private String phoneNumber;
    private String email;
    private Boolean isInternalMember;
    private List<LanguageDto> languages ;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setsurName(String surName) {
        this.surName = surName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getemail() {
        return email;
    }

    public Boolean getisInternalMember() {
        return isInternalMember;
    }

    public void setisInternalMember(Boolean isInternalMember) {
        this.isInternalMember = isInternalMember;
    }

    public List<LanguageDto> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageDto> languages) {
        this.languages = languages;
    }

}

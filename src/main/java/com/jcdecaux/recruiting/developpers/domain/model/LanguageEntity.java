package com.jcdecaux.recruiting.developpers.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name = "Language")
public class LanguageEntity implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String version;
    @JsonIgnore
    @ManyToMany(mappedBy = "languages", fetch = FetchType.LAZY)
    private List<DeveloperEntity> developers = new ArrayList<DeveloperEntity>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<DeveloperEntity> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<DeveloperEntity> developers) {
        this.developers = developers;
    }
}
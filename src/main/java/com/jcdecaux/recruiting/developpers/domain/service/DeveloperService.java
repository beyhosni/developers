package com.jcdecaux.recruiting.developpers.domain.service;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperLanguageDto;
import org.springframework.stereotype.Service;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperDto;
import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;

import javax.ws.rs.QueryParam;
import java.util.List;

@Service
public interface DeveloperService {

    public DeveloperEntity save(DeveloperEntity developer);

    public DeveloperEntity get(Long id);

    public DeveloperEntity save(DeveloperLanguageDto developerLanguageDto);

    public List<DeveloperEntity> getDevsByLanguageId(Long languageId);

}

package com.jcdecaux.recruiting.developpers.domain.mapper;

import com.jcdecaux.recruiting.developpers.domain.dto.LanguageDto;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;

public class LanguageMapper {
    public static LanguageDto fromEntityToDto (LanguageEntity languageEntity){
        LanguageDto languageDto = new LanguageDto();
        languageDto.setId(languageEntity.getId());
        languageDto.setName(languageEntity.getName());
        languageDto.setVersion(languageEntity.getVersion());
        return  languageDto;

    }

    public static LanguageEntity fromDtoToEntity (LanguageDto languageDto){
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setId(languageDto.getId());
        languageEntity.setName(languageDto.getName());
        languageEntity.setVersion(languageDto.getVersion());
        return  languageEntity;

    }

}

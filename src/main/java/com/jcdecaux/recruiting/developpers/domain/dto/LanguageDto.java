package com.jcdecaux.recruiting.developpers.domain.dto;

import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LanguageDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String version;
    private List<DeveloperEntity> developers = new ArrayList<DeveloperEntity>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<DeveloperEntity> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<DeveloperEntity> developers) {
        this.developers = developers;
    }
}

package com.jcdecaux.recruiting.developpers.domain.model;
import javax.persistence.JoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Developer")
public class DeveloperEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String surName;
	private String phoneNumber;
	private String email;
	private Boolean isInternalMember;
	 @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	    @JoinTable(
	        name = "DeveloperLanguage", 
	        joinColumns = { @JoinColumn(name = "developer_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "language_id") }
	    )
	 private List<LanguageEntity> languages ;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setsurName(String surName) {
		this.surName = surName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getemail() {
		return email;
	}

	public Boolean getisInternalMember() {
		return isInternalMember;
	}

	public void setisInternalMember(Boolean isInternalMember) {
		this.isInternalMember = isInternalMember;
	}

	public List<LanguageEntity> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageEntity> languages) {
		this.languages = languages;
	}




}

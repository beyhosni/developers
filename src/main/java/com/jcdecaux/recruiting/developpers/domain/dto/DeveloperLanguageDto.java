package com.jcdecaux.recruiting.developpers.domain.dto;

import java.io.Serializable;
import java.util.Set;

public class DeveloperLanguageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long idDeveloper;
    private Set<Long> idLanguage;


    public Long getIdDeveloper() {
        return idDeveloper;
    }

    public void setIdDeveloper(Long idDeveloper) {
        this.idDeveloper = idDeveloper;
    }

    public  Set<Long> getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(Set<Long> idLanguage) {
        this.idLanguage = idLanguage;
    }
}

package com.jcdecaux.recruiting.developpers.domain.mapper;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperDto;
import com.jcdecaux.recruiting.developpers.domain.dto.LanguageDto;
import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class DeveloperMapper {

    public static DeveloperDto fromEntityToDto (DeveloperEntity developerEntity){
        DeveloperDto developerDto = new DeveloperDto();
        developerDto.setId(developerEntity.getId());
        developerDto.setEmail(developerEntity.getemail());
        developerDto.setisInternalMember(developerEntity.getisInternalMember());
        developerDto.setName(developerEntity.getName());
        developerDto.setsurName(developerEntity.getSurName());
        developerDto.setPhoneNumber(developerEntity.getPhoneNumber());
        if (developerEntity.getLanguages() != null) {
        	List<LanguageDto> languages = new ArrayList<>();
            developerEntity.getLanguages().forEach(language -> {
                languages.add(LanguageMapper.fromEntityToDto(language));
            });
            developerDto.setLanguages(languages);
        }

        return  developerDto;

    }

    public static DeveloperEntity fromDtoToEntity (DeveloperDto developerDto){
        DeveloperEntity developerEntity = new DeveloperEntity();
        developerEntity.setId(developerDto.getId());
        developerEntity.setEmail(developerDto.getemail());
        developerEntity.setisInternalMember(developerDto.getisInternalMember());
        developerEntity.setName(developerDto.getName());
        developerEntity.setsurName(developerDto.getSurName());
        developerEntity.setPhoneNumber(developerDto.getPhoneNumber());
        if (developerDto.getLanguages() != null) {
        	List<LanguageEntity> languages = new ArrayList<>();
            developerDto.getLanguages().forEach(language -> {
                languages.add(LanguageMapper.fromDtoToEntity(language));
            });
            developerEntity.setLanguages(languages);
        }
        

        return  developerEntity;

    }


}

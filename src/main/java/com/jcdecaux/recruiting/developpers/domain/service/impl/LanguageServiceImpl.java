package com.jcdecaux.recruiting.developpers.domain.service.impl;

import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import com.jcdecaux.recruiting.developpers.domain.repository.LanguageRepository;
import com.jcdecaux.recruiting.developpers.domain.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    LanguageRepository languageRepository;


    public LanguageEntity save(LanguageEntity language){
        return languageRepository.save(language);
    }

    public LanguageEntity get(Long id){
        return null;
    }

}

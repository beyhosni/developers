package com.jcdecaux.recruiting.developpers.domain.repository;

import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface LanguageRepository extends Repository<LanguageEntity, String> {

    @Query("SELECT i.id FROM LanguageEntity i")
    String get();

    LanguageEntity save(LanguageEntity languageEntity);

    LanguageEntity findFirstById (Long id);
}

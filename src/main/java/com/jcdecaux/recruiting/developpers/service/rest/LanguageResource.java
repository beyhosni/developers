package com.jcdecaux.recruiting.developpers.service.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.jcdecaux.recruiting.developpers.domain.dto.LanguageDto;
import org.springframework.stereotype.Service;

@Path("/language")
@Consumes("application/json")
@Produces("application/json")
@Service
public interface LanguageResource {

    @POST
    public LanguageDto save(LanguageDto language);



    @GET
    public String get();


}
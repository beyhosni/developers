package com.jcdecaux.recruiting.developpers.service.rest;

import javax.ws.rs.*;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperDto;
import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperLanguageDto;

import com.jcdecaux.recruiting.developpers.domain.dto.LanguageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Path("/developer")
@Consumes("application/json")
@Produces("application/json")
@Service
public interface DeveloperResource {

    @POST
    public DeveloperDto save(DeveloperDto developer);
   
    @Path("/addLanguage")
    @POST
    public DeveloperDto save(DeveloperLanguageDto developerLanguageDto);



    @GET
    public List<DeveloperDto> get(@QueryParam("laguangeId") Long languageId);


}
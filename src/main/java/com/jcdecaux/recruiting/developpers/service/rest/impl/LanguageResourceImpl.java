package com.jcdecaux.recruiting.developpers.service.rest.impl;

import com.jcdecaux.recruiting.developpers.domain.dto.LanguageDto;
import com.jcdecaux.recruiting.developpers.domain.mapper.LanguageMapper;
import com.jcdecaux.recruiting.developpers.domain.model.LanguageEntity;
import com.jcdecaux.recruiting.developpers.service.rest.LanguageResource;
import com.jcdecaux.recruiting.developpers.domain.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageResourceImpl implements LanguageResource  {

    @Autowired
    LanguageService languageService;



    public LanguageDto save(LanguageDto language) {
        LanguageEntity languageEntity = LanguageMapper.fromDtoToEntity(language);
        return  LanguageMapper.fromEntityToDto(languageService.save(languageEntity));
    }

    public String get(){

        return "string" ;

    }


}


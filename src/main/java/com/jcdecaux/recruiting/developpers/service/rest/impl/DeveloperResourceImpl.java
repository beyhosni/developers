package com.jcdecaux.recruiting.developpers.service.rest.impl;

import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperDto;
import com.jcdecaux.recruiting.developpers.domain.dto.DeveloperLanguageDto;
import com.jcdecaux.recruiting.developpers.domain.mapper.DeveloperMapper;
import com.jcdecaux.recruiting.developpers.domain.model.DeveloperEntity;
import com.jcdecaux.recruiting.developpers.domain.service.DeveloperService;
import com.jcdecaux.recruiting.developpers.service.rest.DeveloperResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeveloperResourceImpl implements DeveloperResource  {

    @Autowired
    DeveloperService developerService;

    @Autowired
    DeveloperMapper developerMapper;


    public DeveloperDto save(DeveloperDto developer) {
        DeveloperEntity developerEntity = DeveloperMapper.fromDtoToEntity(developer);
        return  DeveloperMapper.fromEntityToDto(developerService.save(developerEntity));
    }

    public DeveloperDto save(DeveloperLanguageDto developerLanguageDto) {
        return DeveloperMapper.fromEntityToDto(developerService.save(developerLanguageDto));
    }

    public List<DeveloperDto> get(Long languageId){
        List<DeveloperEntity> developersEntities = developerService.getDevsByLanguageId(languageId);
        List<DeveloperDto> developersDtos = new ArrayList<>();
        developersEntities.forEach(dev -> developersDtos.add(DeveloperMapper.fromEntityToDto(dev)));
        return developersDtos ;

    }


}

Developers REST API : This repository provides example of Restful API building with springboot framework. It describe the relation between developers and the programmingLanguages.

Features RESTful routing Models with proper relationships Controllers/Models etc with proper separation of concerns

Developement-Environnement: eclipse STS Spring-boot,java8,maven 3.5.3, H2(database) postman

RestAPi URL:

Creation-Developer: http://localhost:8080/developer Post-Request-example: body { "isInternalMember": true, "surName": "bey", "phoneNumber": "0656727566", "name": "hosni", "email": "hosni@gmail.com" }

modify the developer informations:http://localhost:8080/developer update-request-example:body { "isInternalMember": false, "surName": "bey", "phoneNumber": "0656727566", "name": "hosni", "email": "hosnibey@gmail.com" }

Creation-Programming Language: http://localhost:8080/language/ Post-Request-example: body { "name":"java" "version":"8" }

Associate programming-language to a developer:http://localhost:8080/developer/addLanguage
post-param-request-example:{ "idDeveloper" : 2 ,"idLanguage" : [2 , 3]}

get Developer with there programmingLanguage: get-request-example: http://localhost:8080/developer?idLanguage=2